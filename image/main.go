package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"

	"gopkg.in/yaml.v2"
)

const cfgPath = "/config/content.yaml"
const scrtPath = "/secrets/password.yaml"

var info struct {
	Name string
	Author string
}

var secret struct {
	Password string
}

func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Project: %s Author: %s, Secret: %s", info.Name, info.Author, secret.Password)
}

func main() {
	cfgData, err := ioutil.ReadFile(cfgPath)
	if err != nil {
		log.Fatal(err)
	}
	if err = yaml.Unmarshal(cfgData, &info); err != nil {
		log.Fatalf("failed to parse config file %q: %v", cfgPath, err)
	}

	scrtData, err := ioutil.ReadFile(scrtPath)
	if err != nil {
		log.Fatal(err)
	}
	if err = yaml.Unmarshal(scrtData, &secret); err != nil {
		log.Fatalf("failed to parse config file %q: %v", scrtPath, err)
	}


	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe(":8888", nil))
}